# 项目整体设计

![](ts.png)
## 更新历史
更新时间 | 更新原因
:-:|-
20191201|第一版
## 按功能

### 用户功能部分

#### 登录功能

##### Client

* 实现登录界面，获取用户输入的用户ID，密码
* 将用户ID和密码通过API发送到后台
* 获取后台的响应
* 解析后台返回的响应，若是登录成功则跳转到主界面，否则停留在登录界面并给用户提示

##### API

* 请求格式
    * 方式：HTTP  POST
    * 请求地址：http:sixgod.huang-zh.dev/user_service
    * 参数：
        * flag = login
        * id = 用户输入的ID
        * password = 用户输入的密码经过MD5加密的结果
* 返回格式
    * 方式：HTTP JSON
    * JSON格式：
        ```
        {
            "status_code":1010,
            "data":["user_id":"用户的ID(String),
                    "user_name":用户名(String),
                    "is_admin":是否是用户管理员，是：true，否false,
                    "jsessionid":需要保存该字段，以后几乎每一次操作都会用到]
        }
        ```

##### Controller

* UserServiceController.doPost(...)
    * 从HttpRequest中获取提交的参数
    * 解析用户参数，调用UserService.login(String userID,String password)
    * 根据login(...)方法的返回，设置session状态，生成返回JSON
    * 将JSON返回

##### Service

* UserService.login(String userID,String password)
    * 检查userID，password是否符合要求
        * 若不符合要求可返回null，也可以通过抛出Exception的方式通知Controller层
    * 调用UserDao.searchById(String id)获取userID在数据库中对应的记录所形成的User对象
    * 若UserDao.searchById(...)返回了null，则直接返回null
    * 若UserDao.searchById(...)返回了User对象则对比password与User对象中的password是否一致
        * 若一致，则返回User对象
        * 否则，返回null

##### DAO

* UserDao.searchById(String id)
    * 根据用户id在数据库中搜索对应的记录
    * 若存在对应的记录则根据对应的记录生成User对象，并返回该User对象，否则返回null

#### 设立管理员账户
* 第一个管理员由系统部署时手工在数据库中建立
* 若需要新建管理员账户可通过现有的管理员账户在新建用户功能中选择新建用户的权限为管理员

#### 创建新用户

##### Client

* 设立新建用户页面并提供进入该页面的入口
* 只有管理员才能有入口进入该页面
* 在新建用户页面中，收集管理员用户输入的用户名、用户密码、用户权限
* 将收集到的信息通过API发送到服务器
* 接收服务器返回的响应JSON
* 根据JSON的内容给用户反馈

##### API

* 请求格式
    * 方式：HTTP  POST
    * 请求地址：http:sixgod.huang-zh.dev/user_service
    * 参数：
        * flag = create_user
        * user_name = 用户输入的新用户名
        * password = 用户输入的新用户的密码经过MD5加密的结果
        * jsessionid = 用户登录时的服务诶返回的jsessionid
* 返回格式
    * 方式：HTTP JSON
    * JSON格式：
        * 创建成功：
            ```
            {
                "status_code":1020,
                "data":["user_id":"新用户的ID(String),
                        "user_name":新用户名(String),
                        "is_admin":新用户是否是用户管理员，是：true，否false,
                        "jsessionid":需要保存该字段，以后几乎每一次操作都会用到]
            }
            ```
        * 创建失败：
            ```
            {
                "status_code":错误码（1022：无权限；1021：其他错误），
                "data":[]
            }
            ```
##### Controller

* UserServiceController.doPost(...)
    * 从HttpRequest中获取提交的参数
    * 解析用户参数
    * 判断参数是否齐全
    * 根据session获取已登录的用户对象
    * 检查是否具有权限，若无权限则返回对应的JSON
    * 调用UserService.createUser(String userName,String password,String isAdmin)
    * 根据createUser(...)方法的返回，生成返回JSON
    * 将JSON返回

##### Service

* UserService.createUser(String userName,String password,String isAdmin)
    * 检查userName，password是否符合要求
        * 若不符合要求可返回null，也可以通过抛出Exception的方式通知Controller层
    * 调用UserDao.addUser(String userName,String password,String isAdmin)
    * 若UserDao.createUser(...)返回了null，则直接返回null
    * 若UserDao.createUser(...)返回了User对象返回该User对象

##### DAO

* UserDao.addUser(String userName,String password,String isAdmin)
    * 根据userName，password，isAdmin在数据库中新建一条用户记录，若创建失败，返回null
    * 返回带有数据库自动生成ID的User对象返回

#### 修改用户信息

##### 说明

修改用户信息暂定只修改用户的用户名和密码

##### Client

* 提供用户修改信息的界面

##### API

* 请求格式
    * 方式：HTTP  POST
    * 请求地址：http:sixgod.huang-zh.dev/user_service
    * 参数：
        * flag = alter_user_info
        * alter_element = 需要修改的项目（用户名：USER_NAME，密码：PASSWORD）
        * new_value = 新的值
        * jsessionid = 登录时获得的jsessionid
* 返回格式
    * 方式：HTTP JSON
    * JSON格式：
        * 修改成功：
            ```
            {
                "status_code":1030,
                "data":["user_id":用户的ID(String),
                        "user_name":新用户名(String),
                        "jsessionid":需要保存该字段，以后几乎每一次操作都会用到]
            }
            ```
        * 修改失败：
            ```
            {
                "status_code":错误码（1031：其他错误），
                "data":[]
            }
            ```

##### Controller

* UserServiceController.doPost(...)
    * 从HttpRequest中获取提交的参数
    * 解析用户参数
    * 检查参数，若有错则返回错误JSON
    * 根据session获取已登录的用户对象
    * 调用UserService.alterUserInfo(String userID，String alterElement，String newValue)
    * 根据alterUserInfo(...)方法的返回，生成返回JSON
    * 将JSON返回

##### Service

* UserService.alterUserInfo(String userID，String alterElement，String newValue)
    * 检查参数
    * 调用UserDao.alter(String userID,String alterElement,String newValue)
    * 若alter(...)返回了User对象，则将此User对象直接返回
    * 否则，返回null

##### DAO

* UserDao.alter(String userID,String alterElement,String newValue)
    * 将对应UserID对应的记录的alterElement属性的值改为newValue
    * 若修改成功，则返回修改成功的User对象，否则返回null

### 货物功能

#### 获取货品信息

##### 说明

获取目前所有货品的信息

##### Client

可以选择做一个呈现所有货品的界面

##### API（待完善）

* 请求格式
    * 方式：HTTP  POST
    * 请求地址：http:sixgod.huang-zh.dev/goods_service
    * 参数：
        * flag = get_all_goods
        * jsessionid = 登录时获得的jsessionid
* 返回格式
    * 方式：HTTP JSON
    * JSON格式：
        * 获取成功：
            ```
            {
                "status_code":2010,
                "data":[{"goods_barcode":货物条形码,
                         "goods_name":货物名称,
                         "amount":货物数量,
                         "price":货物价格,
                         "discount":折扣},...]
            }
            ```
        * 获取失败：
            ```
            {
                "status_code":错误码（2011：其他错误），
                "data":[]
            }
            ```

##### Controller

* 通过session检测用户有没有登录，若无直接返回错误JSON
* 调用GoodsService.getAllGoods()
* 根据返回构造JSON

##### Service
* GoodsService.getAllGoods()
    * 调用GoodsDao.getAll()
    * 将结果返回

##### DAO
* GoodsDao.getAllGoods()
    * 查询数据库以Goods数组对象的形式返回结果
    * 若查询出错则返回null

#### 创建货物

##### Client

* 一个可以让用户输入货物条形码、名称、数量、价格、折扣的页面
* 收集到用户输入后通过API发送给后台

##### API

* 请求格式
    * 方式：HTTP  POST
    * 请求地址：http:sixgod.huang-zh.dev/goods_service
    * 参数：
        * flag = create_goods
        * barcode = 货物条形码
        * goods_name = 货物名称
        * amount = 货物数量
        * price = 货物单价
        * discount = 折扣
        * jsessionid = 登录时获得的jsessionid
* 返回格式
    * 方式：HTTP JSON
    * JSON格式：
        * 创建成功：
            ```
            {
                "status_code":2020,
                "data":[{"goods_barcode":货物条形码,
                         "goods_name":货物名称,
                         "amount":货物数量,
                         "price":货物价格,
                         "discount":折扣}]
            }
            ```
        * 创建失败：
            ```
            {
                "status_code":错误码（2011：其他错误），
                "data":[]
            }
            ```

##### Controller

* 获取用户请求
* 提取请求参数
* 检查参数格式是否正确
* 调用GoodsService.createGoods(String barcode,String name,int amount,int price int discount)
* 根据createGoods(...)的返回值确定如何构造JSON

##### Service

* GoodsService.createGoods(String barcode,String name,int amount,int price int discount)
    * 检查参数，若有错则返回null或抛出Exception
    * 